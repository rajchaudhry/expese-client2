import React from 'react';

import { Button, Checkbox, Icon, Modal, Image, Header, Form,
  Responsive, Sidebar, Menu, Segment, Container, Visibility, Dimmer, Loader 
} from 'semantic-ui-react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router';
// import the error message 
import { FormErrors } from './FormErrors';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
// Import Style
import styles from './Account.css';
import SignupModal from './SignupModal.js'
import {Login} from '../../actions/accountAction';
import * as accountAction from '../../actions/accountAction';

const imgS3URL = 'https://s3.amazonaws.com/expese-images/admin/assets/uploads/files/';

class NestedModal extends React.Component {

  state = { email: '', password: '', submittedEmail: '', submittedPassword: '', statusMsg: 'I am status message', formErrors: {email: '', password: ''}, emailValid: false, passwordValid: false, open:false, showModal: true}

  open = () => this.setState({ open: true })
  close = () => this.setState({ open: false })

  validateField(fieldName, value) {
  let fieldValidationErrors = this.state.formErrors;
  let emailValid = this.state.emailValid;
  let passwordValid = this.state.passwordValid;

  switch(fieldName) {
    case 'email':
      emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
      fieldValidationErrors.email = emailValid ? '' : ' is invalid';
      break;
    case 'password':
      passwordValid = value.length >= 6;
      fieldValidationErrors.password = passwordValid ? '': ' is too short';
      break;
    default:
      break;
  }

  this.setState({emailValid: emailValid,
                 passwordValid: passwordValid
              }, this.validateForm);
  }

  validateForm() {
    this.setState({formValid: this.state.emailValid && this.state.passwordValid});
  }

  
  handleChange = (e, { name, value }) => this.setState({ [name]: value }, () => { this.validateField(name, value) })

  handleSubmit = () => {
    const { email, password } = this.state

    this.setState({ submittedEmail: email, submittedPassword: password, statusMsg: 'Logged in!'})
    this.props.Login(email, password);
    console.log("dispatched!!!!")
  }
  
  checkForm() {
    var checkBox = document.getElementById("termCheck");
    var text = document.getElementById("termCheck");
    text.style.display = "none";
    if(checkBox.checked == true) {
      return true;
    }
    else{
      alert("Please indicate that you accept the Terms and Conditions");
      return false;
    }
  } 

  render() {
    const { open } = this.state;
    const { email, password, submittedEmail, submittedPassword, statusMsg } = this.state
    let {isLoginPending, isLoginSuccess, loginError} = this.props;
    console.log("wei this.props", this.props);

    if (isLoginSuccess) {
      console.log("redirecting the page to productSearch");
      //this.props.history.push("/productSearch");
      //return ({Redirect to="/productSearch"/});
      window.location.href='/productSearch';
    }

    return (
      <Modal
        className="scrolling"
        dimmer={true}
        open={open}
        onOpen={this.open}
        onClose={this.close}
        size='small'
        trigger={<Button primary icon>Log in<Icon name='right chevron' /></Button>}
      >
        <div className="container productmain">
        <div className="panel panel-default">
          <FormErrors formErrors={this.state.formErrors} />
        </div>

        <h3> Log in to existing account</h3>
        <Modal.Actions>
          <Button icon='check' content='Back' onClick={this.close} />
        </Modal.Actions>

        <Form id="SignupForm" onSubmit={this.handleSubmit}>
          <Form.Input placeholder='Email' name='email' value={email} onChange={this.handleChange} />
          <Form.Input type='password' placeholder='Password' name='password' value={password} onChange={this.handleChange} />
          <Button color='purple' type='submit' disabled={!this.state.formValid} onclick="this.checkForm" >Let's Go
          </Button>

          <p>
          <label>{statusMsg}</label>
          </p>
          <p>
          <a href="https://www.google.com">Forgot Your Password?</a>
          <br />
          <SignupModal show={this.state.showModal} container={this}/>
          {statusMsg.length ? (
            <a color='purple' type='submit'><Link style={{color: '#fff'}} to={'/products'}>Products</Link>
            </a>

          ) : '' }</p>
        </Form>

        <Menu.Item>
          <Button color='navy' style={{ marginLeft: '1em' }}><Link to={'/'}>Facebook</Link></Button>
          <Button color='mediumblue' style={{ marginLeft: '1em' }}><Link to={'/'}>Twitter</Link></Button>
          <Button color='red' style={{ marginLeft: '1em' }}><Link to={'/'}>S Google</Link></Button>
        </Menu.Item>

      </div>
      </Modal>
    )
  }
}

const mapStateToProps = (state) => {
  console.log("mapStateToProps", state.accountState);
  return {
    isLoginPending: state.accountState.isLoginPending,
    isLoginSuccess: state.accountState.isLoginSuccess,
    loginError: state.accountState.loginError
  };
}

const mapDispatchToProps = (dispatch) => {
  console.log("mapDispatchToProps");
  return bindActionCreators({Login},dispatch)

}
export default withRouter(connect(mapStateToProps,mapDispatchToProps)(NestedModal));

