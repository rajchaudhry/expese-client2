// ./react-redux-client/src/components/Todos.js
import React from 'react';
// import { Alert,Glyphicon,Button,Modal } from 'react-bootstrap';
import { Link } from 'react-router';

import 'react-responsive-carousel/lib/styles/carousel.min.css';

// Import Style
import styles from './Account.css';

// import ProductCard from '../ProductCard/ProductCard';
import { Card, Icon, Image, Segment, Dimmer, Loader } from 'semantic-ui-react'
import { Button, Checkbox, Form, Input, Modal } from 'semantic-ui-react'

const imgS3URL = 'https://s3.amazonaws.com/expese-images/admin/assets/uploads/files/';

export default class Account extends React.Component {
  state = { email: '', password: '', submittedEmail: '', submittedPassword: '', statusMsg: 'blah' }

  componentWillMount(){console.log(' in will mount fetchProductDetail ')
    // this.props.fetchProductDetail(this.props.params.id);
  }

  renderLander() {
    return (
      <div className="lander">
        <Segment>
        <Dimmer active inverted>
          <Loader size='mini'>Loading</Loader>
        </Dimmer>
        </Segment>
      </div>
    );
  }

  renderProductDetail(product) {
    console.log('rendering here')

    if (product.length === 0) {
      return (<div className="col-xs-12">No details found</div>);
    } else {

      return(

          <Segment padded='very'>
            <h1>{product[0]['item_name']}</h1>
            <Image src={imgS3URL + product[0]['image']} />
          </Segment>

      );
    }
  }


  submitSignup(e) {
    e.preventDefault();
    const signupForm = document.getElementById('SignupForm');
console.log(signupForm)
    if(signupForm.email.value !== ""){
      const data = new FormData();
      data.append('email', signupForm.email.value);
      data.append('password', signupForm.password.value);
      // this.props.mappedEditTodo(data);
    }
    else{
      return;
    }
  }

  handleChange = (e, { name, value }) => this.setState({ [name]: value })

  handleSubmit = () => {
    const { email, password } = this.state

    this.setState({ submittedEmail: email, submittedPassword: password, statusMsg: 'Signed up!'})

  }

  render(){
    const productState = this.props.mappedProductState;
    const products = productState.products;
    const product = ( productState.product !== null ) ? productState.product : [];
    const { email, password, submittedEmail, submittedPassword, statusMsg } = this.state

    var w = window.innerWidth + 'px';console.log(w)

    return(
      <div className="container productmain">
        <Form id="SignupForm" onSubmit={this.handleSubmit}>
          <Form.Input placeholder='Email' name='email' value={email} onChange={this.handleChange} />
          <Form.Input type='password' placeholder='Password' name='password' value={password} onChange={this.handleChange} />

          <Form.Field>
            <Checkbox label='I agree to the Terms and Conditions' checked />
          </Form.Field>
          <Button color='purple' type='submit'>submit
          </Button>

          <p>
          <label>{statusMsg}</label>
          </p>
          <p>
          {statusMsg.length ? (
            <Button color='purple' type='submit'><Link style={{color: '#fff'}} to={'/products'}>Products</Link>
            </Button>

          ) : '' }</p>
        </Form>

      </div>
    );
  }
}
// <ProductCard product={product[0]} key={1} />


        // <Modal trigger={<Button>trigger</Button>} basic size='small'>
        //
        //   <Modal.Content>
        //     <p>Signed up!</p>
        //   </Modal.Content>
        //   <Modal.Actions>
        //     <Button color='green' inverted>
        //       <Icon name='checkmark' /> Yes
        //       <Link style={{color: '#fff'}} to={'/products'}>Go to products</Link>
        //     </Button>
        //   </Modal.Actions>
        // </Modal>
