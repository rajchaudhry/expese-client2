import React from 'react';

import { Button, Checkbox, Icon, Modal, Image, Header, Form,
  Responsive, Sidebar, Menu, Segment, Container, Visibility, Dimmer, Loader 
} from 'semantic-ui-react'
import { bindActionCreators } from 'redux'
import { Link,withRouter , Redirect } from 'react-router';
import { connect } from 'react-redux';
//import { Redirect } from 'react-router-dom';
// import the error message 
import { FormErrors } from './FormErrors';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
// Import Style
import styles from './Account.css';
import {Signup} from '../../actions/accountAction';
import * as accountAction from '../../actions/accountAction';

const imgS3URL = 'https://s3.amazonaws.com/expese-images/admin/assets/uploads/files/';

class NestedModal extends React.Component {

  constructor(props){
    super(props);
  }

  state = { email: '', password: '', submittedEmail: '', submittedPassword: '', statusMsg: 'I am status message', formErrors: {email: '', password: ''}, emailValid: false, passwordValid: false, open:false }

  open = () => this.setState({ open: true })
  close = () => this.setState({ open: false })

  renderLander() {
    return (
      <div className="lander">
        <Segment>
        <Dimmer active inverted>
          <Loader size='mini'>Loading</Loader>
        </Dimmer>
        </Segment>
      </div>
    );
  }

  validateField(fieldName, value) {
  let fieldValidationErrors = this.state.formErrors;
  let emailValid = this.state.emailValid;
  let passwordValid = this.state.passwordValid;

  switch(fieldName) {
    case 'email':
      emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
      fieldValidationErrors.email = emailValid ? '' : ' is invalid';
      break;
    case 'password':
      passwordValid = value.length >= 6;
      fieldValidationErrors.password = passwordValid ? '': ' is too short';
      break;
    default:
      break;
  }
  this.setState({emailValid: emailValid,
                 passwordValid: passwordValid
              }, this.validateForm);
  }

  validateForm() {
    this.setState({formValid: this.state.emailValid && this.state.passwordValid});
  }

  
  handleChange = (e, { name, value }) => this.setState({ [name]: value }, () => { this.validateField(name, value) })

  handleSubmit = () => {
    const { email, password } = this.state

    this.setState({ submittedEmail: email, submittedPassword: password, statusMsg: 'Signed up!'})
    this.props.Signup(email, password);
    console.log("dispatched!!!!")
  }
  
  checkForm() {
    var checkBox = document.getElementById("termCheck");
    var text = document.getElementById("termCheck");
    text.style.display = "none";
    if(checkBox.checked == true) {
      return true;
    }
    else{
      alert("Please indicate that you accept the Terms and Conditions");
      return false;
    }
  } 

  render() {
    const { open } = this.state;
    const { email, password, submittedEmail, submittedPassword, statusMsg } = this.state
    let {isLoginPending, isLoginSuccess, loginError} = this.props;
    console.log("wei this.props", this.props);
    
    if (isLoginSuccess) {
      console.log("redirecting the page to productSearch");
      //this.props.history.push("/productSearch");
      //return ({Redirect to="/productSearch"/});
      window.location.href='/productSearch';
    }
    
    return (
      <Modal
        className="scrolling"
        dimmer={true}
        open={open}
        onOpen={this.open}
        onClose={this.close}
        size='small'
        trigger={<Button color='red' primary icon>Get Started<Icon name='right chevron' /></Button>}
        >
        <div className="container productmain">
        <div className="panel panel-default">
          <FormErrors formErrors={this.state.formErrors} />
        </div>

        <Modal.Actions>
          <Button icon='check' content='Back' onClick={this.close} />
        </Modal.Actions>
        <h3> Register for a new account</h3>

        <Form id="SignupForm" onSubmit={this.handleSubmit}>
          <Form.Input placeholder='Email' name='email' value={email} onChange={this.handleChange} />
          <Form.Input type='password' placeholder='Password' name='password' value={password} onChange={this.handleChange} />
          <Form.Field>
            <Checkbox label='I agree to the Terms and Conditions' id = "termCheck"/>
          </Form.Field>
          <Button color='purple' type='submit' disabled={!this.state.formValid} onclick="this.checkForm" >Submit
          </Button>

          <p>
          <label>{statusMsg}</label>
          </p>

          <div className="message">
          { isLoginPending && <div>Please wait...</div> }
          { isLoginSuccess && <Redirect to="/productSearch"/>}
          { loginError && <div>Signup failed, please change your email and password.</div> }
          </div>
        </Form>

        <Menu.Item>
          <Button color='navy' style={{ marginLeft: '1em' }}><Link to={'/'}>Sign Up with Facebook</Link></Button>
          <Button color='mediumblue' style={{ marginLeft: '1em' }}><Link to={'/'}>Sign Up with Twitter</Link></Button>
          <Button color='red' style={{ marginLeft: '1em' }}><Link to={'/'}>Sign Up with Google</Link></Button>
        </Menu.Item>

      </div>
      </Modal>
    )
  }
}

const mapStateToProps = (state) => {
  console.log("mapStateToProps", state.accountState);
  return {
    isLoginPending: state.accountState.isLoginPending,
    isLoginSuccess: state.accountState.isLoginSuccess,
    loginError: state.accountState.loginError
  };
}

const mapDispatchToProps = (dispatch) => {
  console.log("mapDispatchToProps");
  return bindActionCreators({Signup},dispatch)

}
export default withRouter(connect(mapStateToProps,mapDispatchToProps)(NestedModal));

