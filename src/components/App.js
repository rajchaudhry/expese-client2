// ./react-redux-client/src/components/App.js
import React from 'react';
// import { Navbar,Nav,NavItem } from 'react-bootstrap';
// import { LinkContainer,Link } from 'react-router-bootstrap';
import './App.css';
import { Link } from 'react-router';
import MailChimp from "react-mailchimp-subscribe";
import { Button, Icon, Modal, Image, Header,
  Responsive, Sidebar, Menu, Segment, Container, Visibility, Grid, List
} from 'semantic-ui-react';
import Matthew from '../assets/matthew.png'; // colocated image in component folder
// import Queue from '../components/Queue/Queue.js';
import SignupModal from './Account/SignupModal.js'
import LoginModal from './Account/LoginModal.js'
import TodoForm from './TodoForm';

export default class App extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      showModal: false
    };
    this.toggleAddTodo = this.toggleAddTodo.bind(this);
    this.addTodo = this.addTodo.bind(this);
    this.showSignUp = this.showSignUp.bind(this);
  }

  toggleAddTodo(e){
    e.preventDefault();
    this.props.mappedShowSignUp();
  }

  showSignUp(e){console.log(' in htis')
    e.preventDefault();
    this.props.mappedShowSignUp();
  }


  addTodo(e){
      e.preventDefault();
      const form = document.getElementById('addTodoForm');
      if(form.todoText.value !== ""  && form.todoDesc.value !== ""){
        const data = new FormData();
        data.append('todoText', form.todoText.value);
        data.append('todoDesc', form.todoDesc.value);
        this.props.mappedAddTodo(data);
      form.reset();
      }
      else{
        return ;
      }
  }



  handleToggle = () => this.setState({ sidebarOpened: !this.state.sidebarOpened })
  hideFixedMenu = () => this.setState({ fixed: false })
  showFixedMenu = () => this.setState({ fixed: true })

  render() {
    const appState = this.props.mappedAppState;
    const { children } = this.props

    const { fixed } = false;//this.state! =>  {...Responsive.onlyComputer} only for the desktop screen!

    const {submittedEmail} = 'joe@schmoe'


    return (
      <Responsive>
        <Visibility once={false} onBottomPassed={this.showFixedMenu} onBottomPassedReverse={this.hideFixedMenu}>
          <Segment inverted textAlign='center' style={{ padding: '1em 1em' }} vertical>
            <Menu
              fixed={fixed ? 'top' : null}
              inverted={!fixed}
              pointing={!fixed}
              secondary={!fixed}
              size='large'
            >
              <Container>
              <Menu.Item>
                <Link to={'/'}>Expese</Link>
              </Menu.Item>
              <Menu.Item>
                <Button style={{ marginLeft: '1em' }}><Link to={'/queue'}>My List</Link></Button>
              </Menu.Item>
                <Menu.Item position='right'>
                  <Button color='black' style={{ marginLeft: '1em' }}><Link to={'/ProductSearch'}>Products</Link></Button>
                  <SignupModal show={this.state.showModal} container={this}/>
                  <LoginModal show={this.state.showModal} container={this}/>
                </Menu.Item>
                <SignupModal show={this.state.showModal} container={this}/>
                <LoginModal show={this.state.showModal} container={this}/>
              </Container>
            </Menu>

          </Segment>
        </Visibility>
        {children}


        {children}

      <Segment inverted vertical style={{ padding: '5em 0em' }}>
      <Container>
        <Grid divided stackable>
          <Grid.Row>
            <Grid.Column floated = 'left' width={3}>
              <Header inverted as='h4' content='About' />
              <List link inverted>
                <List.Item as='a' href = '/About'>What we Do</List.Item>
                <List.Item as='a' href = 'Contact'>Contact Us</List.Item>
                <List.Item as='a' href = 'FAQs'>FAQ's</List.Item>
                <List.Item as='a'>Privacy Policy </List.Item>
                <List.Item as='a'>Terms of Use</List.Item>
              </List>
            </Grid.Column>
            <Grid.Column width={3}>
              <Header as ='h5' color='grey' content='Newsletter' />
                <MailChimp inverted style={{background: 'white', color: 'black'}} url = {"xxxx.us13.list-manage.com/subscribe/post?u=zefzefzef&id=fnfgn"}/>
            </Grid.Column>
            <Grid.Column floated = 'right' width={5}>
              <Header as='h5' inverted content='Follow Us'/>
                <div>
                  <Button color='facebook' href='//facebook.com/expeseit/'>
                    <Icon name='facebook' />Facebook
                  </Button>
                  <Button color='twitter' href='//twitter.com/expeseit/'>
                    <Icon name='twitter' />Twitter
                  </Button>
                  <Button color='linkedin' href='//www.linkedin.com/company/tab-coin-club.com/'>
                    <Icon name='linkedin' />LinkedIn
                  </Button>
                  <Button color='instagram' href= '//www.instagram.com/expeseit/'>
                    <Icon name='instagram' />Instagram
                  </Button>
                </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </Segment>
      </Responsive>

    );
  }

}