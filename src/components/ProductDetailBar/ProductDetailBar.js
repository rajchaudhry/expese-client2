import React, { Component } from 'react'
import { Menu } from 'semantic-ui-react'

export default class MenuExampleBasic extends Component {
  state = {}

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    const { activeItem } = this.state

    return (
      <Menu>
        <Menu.Item
          name='box'
          active={activeItem === 'box'}
          onClick={this.handleItemClick}
        >
          What's in the Box
        </Menu.Item>

        <Menu.Item
          name='review'
          active={activeItem === 'review'}
          onClick={this.handleItemClick}
        >
          Expeser Reviews
        </Menu.Item>

        <Menu.Item
          name='manual'
          active={activeItem === 'manual'}
          onClick={this.handleItemClick}
        >
          Manual
        </Menu.Item>
      </Menu>
    )
  }
}