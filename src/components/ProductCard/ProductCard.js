import React from 'react'
import { Card, Icon, Image, Button } from 'semantic-ui-react'
import { Link } from 'react-router';

import Matthew from '../../assets/matthew.png' // colocated image in component folder

const ProductCard = (props) => {

  const imageToShow = (props['product'] !== 'undefined') ? 'https://s3.amazonaws.com/expese-images/admin/assets/uploads/files/'+props['product']['image'] // colocated image in component folder
  : Matthew

  const product = props['product'];

  return (
  <Card raised link>
    <Image src={imageToShow} />
    <Card.Content>
      <Card.Header>
        {props['product']['item_name']}
      </Card.Header>
      <Card.Meta>
        <span className='date'>
          Joined in 2015
        </span>
      </Card.Meta>
      <Card.Description>
      {props['product']['item_name']}
      </Card.Description>
    </Card.Content>
    <Card.Content extra>
      <Button basic color='green'><Link to={`/product/${product['product_id']}`}>Details</Link></Button>
    </Card.Content>
  </Card>
)}

export default ProductCard
