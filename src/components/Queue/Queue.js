import React from 'react';
import { bindActionCreators } from 'redux'
import { Link, withRouter, Redirect } from 'react-router';
import { connect } from 'react-redux';
import { Button, Container, Header, Icon, Image, Menu, Message, Table, Segment } from 'semantic-ui-react';

export default class Queue extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      products: [],
      activeItem: 'ALL'
    }
    this.state.products = [
      {
        id: 1,
        name: "DJI Mavic Pro",
        img_src: "",
      }, {
        id: 2,
        name: "Phantom 4",
        img_src: "",
      }, {
        id: 3,
        name: "Drone 2",
        img_src: "",
      }, {
        id: 4,
        name: "Google Pixel Book",
        img_src: "",
      }, {
        id: 5,
        name: "Samsung Watch",
        img_src: "",
      }
    ];
    
    this.handleMenuClick = this.handleMenuClick.bind(this);
  }

  handleMenuClick(e, { name }) {
    this.setState({ activeItem: name });
  }

  submitRemoveItem(product) {
    const products = this.state.products;
    const index = products.indexOf(product);
    products.splice(index, 1);
    this.props.postQueue(1, products); // 1 -> user_id -> localStorage.getItem("user").id
    this.setState({ products: products });
  };
  
  submitMoveUpItem(product) {
    const products = this.state.products;
    const index = products.indexOf(product);
    if (index !== 0) {
      products[index - 1] = products.splice(index, 1, products[index - 1])[0]; // swap with the previous item
      this.props.postQueue(1, products); // 1 -> user_id -> localStorage.getItem("user").id
      this.setState({ products: products });
    }
  }

  submitMoveDownItem(product) {
    const products = this.state.products;
    const index = this.state.products.indexOf(product);
    if (index != this.state.products) {
      products[index + 1] = products.splice(index, 1, products[index + 1])[0]; // swap with the next item
      this.props.postQueue(1, products); // 1 -> user_id -> localStorage.getItem("user").id
      this.setState({ products: products });
    }
  }

  render() {
    const { activeItem } = this.state;

    return (
      <Container>
        <Segment inverted padded color='purple'>
          <Header as='h2'>My List</Header>
        </Segment>

        <Menu pointing secondary>
          <Menu.Item
            name='ALL'
            active={activeItem == 'ALL'}
            onClick={this.handleMenuClick}
          />
          <Menu.Item
            name='AT HOME'
            active={activeItem == 'AT HOME'}
            onClick={this.handleMenuClick}
          />
          <Menu.Item
            name='LIST'
            active={activeItem == 'LIST'}
            onClick={this.handleMenuClick}
          />
          <Menu.Item
            name='HISTORY'
            active={activeItem == 'HISTORY'}
            onClick={this.handleMenuClick}
          />
        </Menu>

        {activeItem == 'ALL' || activeItem == 'AT HOME' ? <AtHomeTable /> : null}

        {activeItem == 'ALL' || activeItem == 'LIST' ? <MyListTable 
          onRowMoveUp={this.submitMoveUpItem.bind(this)}
          onRowMoveDown={this.submitMoveDownItem.bind(this)}
          onRowDel={this.submitRemoveItem.bind(this)}
          products={this.state.products}/> : null}
          
      </Container>
    );
  }
}

class AtHomeTable extends React.Component {
  render() {
    return (
      <div>
        <Header size='large'>At Home</Header>
        <Table fixed>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>GADGET</Table.HeaderCell>
              <Table.HeaderCell>
                RETURN BY
                <Icon circular inverted name='info' size="mini"/>
              </Table.HeaderCell>
              <Table.HeaderCell>PURCHASE</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            <AtHomeTableRow></AtHomeTableRow>
          </Table.Body>
        </Table>
      </div>
    );
  }
}

class AtHomeTableRow extends React.Component {
  render() {
    return (
      <Table.Row>
        <Table.Cell>
          <Header as='h4' image>
            <Image src="" rounded size='mini' />
            <Header.Content>
              Product Name
            </Header.Content>
          </Header>
        </Table.Cell>
        <Table.Cell>
          <p>06/12/18</p>
          <Button color='purple'>Due in 3 Days</Button>
        </Table.Cell>
        <Table.Cell>
          <Button color='yellow'>BUY NOW</Button>
        </Table.Cell>
      </Table.Row>
    );
  }
}

class MyListTable extends React.Component {
  render() {
    const onMyListTableUpdate = this.props.onMyListTableUpdate;
    const rowMoveUp = this.props.onRowMoveUp;
    const rowMoveDown = this.props.onRowMoveDown;
    const rowDel = this.props.onRowDel;
    const body = this.props.products.map(function(product) {
      return (
        <MyListTableRow
          onMyListTableUpdate={onMyListTableUpdate}
          product={product}
          onMoveUpEvent={rowMoveUp.bind(this)}
          onMoveDownEvent={rowMoveDown.bind(this)}
          onDelEvent={rowDel.bind(this)}
          key={product.id}/>
      )
    });

    return (
      <div>
        <Header size='large'>My List</Header>
        <Message>
          <p>Next available product from the list will ship out once return the current rental</p>
        </Message>
        <Table fixed>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell></Table.HeaderCell>
              <Table.HeaderCell>GADGET</Table.HeaderCell>
              <Table.HeaderCell>PURCHASE OPTIONS</Table.HeaderCell>
              <Table.HeaderCell></Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {body}
          </Table.Body>
        </Table>
      </div>
    );
  }
}

class MyListTableRow extends React.Component {
  onMoveUpEvent() { this.props.onMoveUpEvent(this.props.product); }
  onMoveDownEvent() { this.props.onMoveDownEvent(this.props.product); }
  onDelEvent() { this.props.onDelEvent(this.props.product); }

  render() {
    return (
      <Table.Row>
        <Table.Cell>
          {this.props.product.id}
        </Table.Cell>
        <Table.Cell>
          <Header as='h4' image>
            <Image src={this.props.product.img_src} rounded size='mini' />
            <Header.Content>
              {this.props.product.name}
            </Header.Content>
          </Header>
        </Table.Cell>
        <Table.Cell>
          <Button color='yellow'>BUY NOW</Button>
        </Table.Cell>
        <Table.Cell>
          <Icon name='triangle up' size="large" onClick={this.onMoveUpEvent.bind(this)}/>
          <Icon name='triangle down' size="large" onClick={this.onMoveDownEvent.bind(this)}/>
          <Icon circular inverted name='x' onClick={this.onDelEvent.bind(this)} />
        </Table.Cell>
      </Table.Row>
    );
  }
}