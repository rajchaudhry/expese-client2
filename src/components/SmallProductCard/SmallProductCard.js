import React from 'react'
import { Card, Icon, Image, Button } from 'semantic-ui-react'
import { Link } from 'react-router';

import Matthew from '../../assets/matthew.png' // colocated image in component folder

const SmallProductCard = (props) => {

  const imageToShow = (props['product'] !== 'undefined') ? 'https://s3.amazonaws.com/expese-images/admin/assets/uploads/files/'+props['product']['image'] // colocated image in component folder
  : Matthew

  const product = props['product'];

  return (
  <Card raised link>
    <Image src={imageToShow} />
    <Card.Content>
      <Card.Header>
        {props['product']['item_name']}
      </Card.Header>
      <Button basic color='grey' floated='left'><Link to={`/product/${product['product_id']}`}>Add</Link></Button>
      <Button basic color='grey' floated='right'><Link to={`/product/${product['product_id']}`}>Share</Link></Button>
    </Card.Content>
  </Card>
)}

export default SmallProductCard
