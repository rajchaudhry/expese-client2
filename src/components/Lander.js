// ./react-redux-client/src/components/Todos.js
import React from 'react';
import { Link } from 'react-router';
import {Button, Icon, Modal, Image, Header, Responsive, Sidebar, Menu, Segment, Container, Visibility, Grid, Card, List, Form, Message, Divider} from 'semantic-ui-react'

import Logo from '../assets/logo.png'
import Rocket from '../assets/Image.png'
import Reuse from '../assets/Image 2.png'
import Cash from '../assets/Image 3.png'
import Home from '../assets/Image 4.png'

import 'react-responsive-carousel/lib/styles/carousel.min.css';
// <link rel="stylesheet" href="carousel.css"/>

import ExpeseImg1 from '../assets/expese_1.jpg' // colocated image in component folder
import ExpeseImg2 from '../assets/expese_2.jpg' // colocated image in component folder
import ExpeseImg3 from '../assets/expese_3.jpg' // colocated image in component folder
import ExpeseImg4 from '../assets/expese_4.jpg' // colocated image in component folder

// Import Style
import styles from './Todos.css';

var Carousel = require('react-responsive-carousel').Carousel;

export default class Lander extends React.Component {
  constructor(props){
    super(props);
  }


  render(){

    var w = window.innerWidth + 'px';console.log(w)
    var tabData = [
      { name: 'Tab 1', isActive: true },
      { name: 'Tab 2', isActive: false },
      { name: 'Tab 3', isActive: false }
    ];

    return(
      <div>
          <Carousel showStatus={false} width={w-1} autoPlay showThumbs={false} interval={2000} infiniteLoop showArrows={true}>
              <div>
                  <img src={ExpeseImg1} className={styles['carouselimg']}/>
              </div>
              <div>
                  <img src={ExpeseImg2} className={styles['carouselimg']}/>
              </div>
              <div>
                  <img src={ExpeseImg3} className={styles['carouselimg']}/>
              </div>
              <div>
                  <img src={ExpeseImg4} className={styles['carouselimg']}/>
              </div>
          </Carousel>
    <Segment style={{ padding: '4em 0em' }} vertical textAlign='center'>
      <Grid container stackable verticalAlign='middle' centered columns>
        <Grid.Row textAlign='justified'>
          <Grid.Column width={16} textAlign='center'>
            <Header as='h3' style={{ fontSize: '2em' }}>How Does it Work?</Header>
            <Card.Group style={{paddingLeft: '8em'}}>
              <Card>
                <Card.Content>
                  <Card.Header>1. Add to List</Card.Header>
                  <Card.Description>Make your list of the products you are dying to experience</Card.Description>
                </Card.Content>
              </Card>

              <Card>
                <Card.Content>
                  <Card.Header>2. Gets Shipped</Card.Header>
                  <Card.Description>Receive the next available gadget from the list and use it for up to 21 days</Card.Description>
                </Card.Content>
              </Card>

              <Card>
                <Card.Content>
                  <Card.Header>3. Repeat</Card.Header>
                  <Card.Description>Return the device after 21 days, and receive the next one from your list</Card.Description>
                </Card.Content>
              </Card>
            </Card.Group>

            <Header as='h3' style={{ fontSize: '1.5em', fontStyle: "italic" }}>Or Keep it Forever</Header>
            <p style={{ fontSize: '1em' }}>
              If you fall in love with one of your gadgets, you can make it your own for a reduced price.
            </p>
          </Grid.Column>
          <Grid.Column floated='right' width={6}>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column textAlign='center'>
            <Button size='huge' href = '#'>Check Us Out</Button>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Divider
          as='h4'
          className='header'
          horizontal
          style={{ margin: '3em 0em', textTransform: 'uppercase' }}
        >
          <a href='#'>The Benefits of Expese</a>

          </Divider>

          <Grid celled='internally' columns='equal' stackable>
            <Grid.Row textAlign='center'>
              <Grid.Column style={{ paddingBottom: '3em', paddingTop: '3em' }}>
                <Image src = {Rocket} size='tiny' centered/>
                <Header as='h3' style={{ fontSize: '2em' }}>Stay on Top of Technology</Header>
                <p style={{ fontSize: '1em' }}>Now you can try all the newest and coolest tech for a flat monthly fee.</p>
              </Grid.Column>
              <Grid.Column style={{ paddingBottom: '3em', paddingTop: '3em' }}>
                <Image src = {Reuse} size='tiny' centered/>
                <Header as='h3' style={{ fontSize: '2em' }}>Try Before you Buy</Header>
                <p style={{ fontSize: '1em' }}>Expese made it possible to try that expensive drone before buying it. We call that being a smart shopper.</p>
              </Grid.Column>
            </Grid.Row>

            <Grid.Row textAlign='center'>
              <Grid.Column style={{ paddingBottom: '3em', paddingTop: '3em' }}>
                <Image src = {Cash} size='tiny' centered/>
                <Header as='h3' style={{ fontSize: '2em' }}>Save Money</Header>
                <p style={{ fontSize: '1em' }}>Play more, spend less. Expese members save up to 20% on gadget purchases.</p>
              </Grid.Column>
              <Grid.Column style={{ paddingBottom: '3em', paddingTop: '3em' }}>
                <Image src = {Home} size='tiny' centered/>
                <Header as='h3' style={{ fontSize: '2em' }}>Comfort of your own home</Header>
                <p style={{ fontSize: '1em' }}>No need to go to the store or buy and return to try all new products. Experience them on your own terms.</p>
              </Grid.Column>
            </Grid.Row>
          </Grid>
    </Segment>

      </div>
    );
  }
}