// ./react-redux-client/src/components/Todos.js
import React from 'react';
// import { Alert,Glyphicon,Button,Modal } from 'react-bootstrap';
import { Link } from 'react-router';

import 'react-responsive-carousel/lib/styles/carousel.min.css';

// Import Style
import styles from './Products.css';

import ProductCard from '../ProductCard/ProductCard';
import { Card, Icon, Image, Segment, Dimmer, Loader } from 'semantic-ui-react'

import Para from '../../assets/paragraph.png' // colocated image in component folder

export default class Products extends React.Component {
  constructor(props){
    super(props);
    this.hideEditModal = this.hideEditModal.bind(this);
    this.submitEditTodo = this.submitEditTodo.bind(this);
    this.hideDeleteModal = this.hideDeleteModal.bind(this);
    this.cofirmDeleteTodo = this.cofirmDeleteTodo.bind(this);
  }

  componentWillMount(){console.log(' in will mount ')
    this.props.fetchProducts();
  }


  showEditModal(todoToEdit){
     this.props.mappedshowEditModal(todoToEdit);
  }

  hideEditModal(){
     this.props.mappedhideEditModal();
  }

  submitEditTodo(e){
    e.preventDefault();
    const editForm = document.getElementById('EditTodoForm');
    if(editForm.todoText.value !== ""){
      const data = new FormData();
      data.append('id', editForm.id.value);
     data.append('todoText', editForm.todoText.value);
      data.append('todoDesc', editForm.todoDesc.value);
      this.props.mappedEditTodo(data);
    }
    else{
      return;
    }

  }

  hideDeleteModal(){
    this.props.mappedhideDeleteModal();
  }

  showDeleteModal(todoToDelete){
    this.props.mappedshowDeleteModal(todoToDelete);
  }

  cofirmDeleteTodo(){
    this.props.mappedDeleteTodo(this.props.mappedTodoState.todoToDelete);
  }

  renderLander() {
    return (
      <div className="lander">
        <Segment>
        <Dimmer active inverted>
          <Loader size='large'>Loading</Loader>
        </Dimmer>
        <Image src={Para} />
        </Segment>
      </div>
    );
  }

  renderProducts(products) {
    console.log('rendering here')

    if (products.length === 0) {
      return (<div className="col-xs-12">No details found</div>);
    } else {
      return(
          <Card.Group fluid stackable textAlign="center">
          {products.map((product,i) =>
            <ProductCard product={product} key={i} />
            )
          }
          </Card.Group>

      );
    }
  }

  render(){
    const todoState = this.props.mappedTodoState;
    const productState = this.props.mappedProductState;
    const todos = todoState.todos;
    const editTodo = todoState.todoToEdit;
    const products = productState.products;

    var w = window.innerWidth + 'px';console.log(w)

    return(
      <div className="container productmain">

      { productState.isFetching
        ? this.renderLander()
        : this.renderProducts(products) }
      </div>
    );
  }
}
