 import React from 'react';
// import { Alert,Glyphicon,Button,Modal } from 'react-bootstrap';
import { Link } from 'react-router';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
// Import Style
import styles from './ProductDetail.css';
import Products from '../Products/Products';
import ProductCard from '../ProductCard/ProductCard';
import SmallProductCard from '../SmallProductCard/SmallProductCard'; 
import DetailsBar from '../DetailsBar/DetailsBar';
import ReadMore from '../ReadMore/ReadMore';
import { Card, Icon, Image, Segment, Button, Label,
  Dimmer, Loader, Container, Header, Tab, Menu, Rating, Input, Grid} from 'semantic-ui-react';
import Paragraph from '../../assets/paragraph.png';

const imgS3URL = 'https://s3.amazonaws.com/expese-images/admin/assets/uploads/files/';
var Carousel = require('react-responsive-carousel').Carousel;


export default class ProductDetail extends React.Component {

  constructor(props){
    super(props);
  }

  componentWillMount(){console.log(' in will mount fetchProductDetail ')
    this.props.fetchProductDetail(this.props.params.id);
  }
  renderLander() {
    return (
      <div className="lander">
        <Segment>
        <Dimmer active inverted>
          <Loader size='mini'>Loading</Loader>
        </Dimmer>
        </Segment>
      </div>
    );
  }

  renderProductDetail(product) {
    console.log(product)
    if (product.length === 0) {
      return (<div className="col-xs-12">No details found</div>);
    } else {

      return(
        <Segment padded='very'>
          <Carousel showStatus={false} autoPlay showThumbs={true} interval={4000} infiniteLoop showArrows={true}>
            {
              product['image'].split(",").map((img, key) =>
                <div><img src={imgS3URL + img} /></div>
              )
            }
          </Carousel>

          <h1>{product['item_name']}</h1>

          <Grid container divided padded>
            <Grid.Row>
              <Grid.Column width = {3}>By EXPESER</Grid.Column>
              <Grid.Column width = {5}>Community Rating: 
                <Rating icon='star' defaultRating={3} maxRating={5} size='large'></Rating>
              </Grid.Column>
            </Grid.Row>
          </Grid>

            <Button color='black' size="massive"compact>
                <Icon name='add' />
                <Link to={`/product/${product['product_id']}`}>EXPESE IT</Link>
            </Button>

            <Button color='red' size="massive" compact>
                <Icon name='cart' />
                BUY NOW
            </Button>

            <ReadMore>
            
            {<div dangerouslySetInnerHTML={{ __html: product['description'] }} />}
            </ReadMore>

        </Segment>

          
      ); 
    }
  } //btn actions work on 

  renderRecommendations(products){
    console.log('rendering here')

    if (products.length === 0) {
      return (<Segment padded> <div className="col-xs-12">Nothing found</div></Segment>);
    } else {

      return(

        <Segment padded>
        <div>
          <Container textAlign='center'>
            <Header as='h2' > Recommended Products </Header>
          </Container>
        </div>
            <Card.Group doubling stackable textAlign="center">
            {products.map((product,i) =>
              <SmallProductCard product={product} key={i} />
              )
            }
            </Card.Group>
        </Segment>
      );
    }
  }

  state = { activeItem: 'Expeser Review' }
  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  renderExtras(products){
    const { activeItem } = this.state
      return (
      <div>
        <Menu attached='top' tabular>
          <Menu.Item name='Expeser Review' active={activeItem === 'Expeser Review'} onClick={this.handleItemClick}/>
          <Menu.Item
            name='Specs'
            active={activeItem === 'Specs'} onClick={this.handleItemClick}
          />
          <Menu.Item
            name="What's in the Box"
            active={activeItem === "What's in the Box"} onClick={this.handleItemClick}
          />
          <Menu.Item
            name='User Manual'
            active={activeItem === 'User Manual'} onClick={this.handleItemClick}
          />
        </Menu>
        <Segment attached='bottom'>
          <img src='/../assets/paragraph.png' />
        </Segment>
      </div>
    )
  }



  render(){
    const productState = this.props.mappedProductState;
    const prods = productState.products;
    const product = ( productState.product !== null ) ? productState.product : [];

    var w = window.innerWidth + 'px';console.log(w)

    return(
      <div className="container productmain">

      {productState.isFetching && this.renderLander()
      }

      { productState.isFetching === null && product.length < 1
        ? this.renderLander()
        : this.renderProductDetail(product) 
      }

      {this.renderExtras(prods)}
      {this.renderRecommendations(prods)}
      
      </div>

    );
  }
}