import React from 'react';
import { Link, Redirect } from 'react-router';

// Import Style
import styles from './ProductSearch.css';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

// Import Components
import ProductCard from '../ProductCard/ProductCard';
import { Card, Icon, Image, Segment, Dimmer, Loader, Button } from 'semantic-ui-react';
import { ReactiveBase, CategorySearch, SingleRange, MultiDropdownList, ResultCard, SelectedFilters } from '@appbaseio/reactivesearch';
import Para from '../../assets/paragraph.png' // colocated image in component folder

export default class ProductSearch extends React.Component {
  constructor(props){
    super(props);
    this.submitAddToQueue = this.submitAddToQueue.bind(this);
    this.hideEditModal = this.hideEditModal.bind(this);
    this.submitEditTodo = this.submitEditTodo.bind(this);
    this.hideDeleteModal = this.hideDeleteModal.bind(this);
    this.cofirmDeleteTodo = this.cofirmDeleteTodo.bind(this);
  }

  componentWillMount(){console.log(' in will mount ')
    this.props.fetchProducts();
  }

  submitAddToQueue(e, productId){
    e.preventDefault();

    const user = localStorage.getItem("user");
    if (user && user.token) {
      this.props.addToQueue();
    } else {
      return (
        <Redirect to={'/account'} />
      )
    }
  }

  showEditModal(todoToEdit){
     this.props.mappedshowEditModal(todoToEdit);
  }

  hideEditModal(){
     this.props.mappedhideEditModal();
  }

  submitEditTodo(e){
    e.preventDefault();
    const editForm = document.getElementById('EditTodoForm');
    if(editForm.todoText.value !== ""){
      const data = new FormData();
      data.append('id', editForm.id.value);
     data.append('todoText', editForm.todoText.value);
      data.append('todoDesc', editForm.todoDesc.value);
      this.props.mappedEditTodo(data);
    }
    else{
      return;
    }

  }

  hideDeleteModal(){
    this.props.mappedhideDeleteModal();
  }

  showDeleteModal(todoToDelete){
    this.props.mappedshowDeleteModal(todoToDelete);
  }

  cofirmDeleteTodo(){
    this.props.mappedDeleteTodo(this.props.mappedTodoState.todoToDelete);
  }

  renderLander() {
    return (
      <div className="lander">
        <Segment>
        <Dimmer active inverted>
          <Loader size='large'>Loading</Loader>
        </Dimmer>
        <Image src={Para} />
        </Segment>
      </div>
    );
  }

  renderProducts(products) {
    console.log('rendering here')

    if (products.length === 0) {
      return (<div className="col-xs-12">No details found</div>);
    } else {

      return(

        <ReactiveBase
  				app="expese_v2"
  				credentials="PbogyeeR0:1d35a365-3763-4fe0-b94e-11b72e89c8da">

          <div style={{ display: "flex", flexDirection: "column", margin: "0 auto"}}>
            <div style={{ display: "flex", flexDirection: "column", width: "100%", marginBottom: '10px', margin: "0 auto"}}>

              <SingleRange 
                componentId="rating"
                title="Ratings"
                dataField="rating" // products does not contain a rating field
                data={[
                  { start: 4, end: 5, label: '★★★★ & up' },
                  { start: 3, end: 5, label: '★★★ & up' },
                  { start: 2, end: 5, label: '★★ & up' },
                  { start: 1, end: 5, label: '★ & up' }
                ]}
                URLParams={true}
                filterLabel="Ratings"
              />

              <MultiDropdownList
                componentId="brand"
                title="Gadgets Brand"
                dataField="brand_name_id" // should be brand's display name instead of id
                URLParams={true}
                filterLabel="Brands"
              />

              <MultiDropdownList
                componentId="category"
                title="Gadgets Category"
                dataField="category_name_id" // should be category's display name instead of id
                URLParams={true}
                filterLabel="Categories"
              />
              
              <CategorySearch
                componentId="gadgets"
                title="Gadgets search"
                dataField={['item_name', 'item_name.search']}
                categoryField="item_name.raw"
                placeholder="Search for gadgets"
                autosuggest={false}
                URLParams={true}
                filterLabel="Gadgets"
              />
              </div>

              <SelectedFilters />
              <ResultCard
                componentId="result"
                title="Results"
                dataField="item_name"
                from={0}
                size={100}
                pagination={false}
                react={{
                  and: ["gadgets", "rating", "category", "brand"]
                }}
                onData={(res) => {
                  return {
                    image: "https://s3.amazonaws.com/expese-images/admin/assets/uploads/files/" + res.image,
                    title: res.item_name,
                    description: (
                      <div>
                        <span>res.description</span><br/>   {/* res.description */}
                        <span>{'★'.repeat(5)}</span><br/>   {/* res.rating*/}
                        <Button onClick={(e) => this.submitAddToQueue(e, res.product_id)}>Add</Button>
                        <Button><Link to={'/product/{res.product_id}'}>Details</Link></Button>
                      </div>
                    ),
                  }
                }}
                sortOptions={[
                  {
                    dataField: 'quantity',
                    sortBy: 'asc',
                    label: 'Quantity (ascending)',
                  },
                  {
                    dataField: 'quantity',
                    sortBy: 'desc',
                    label: 'Quantity (descending)',
                  },
                ]}
                onNoResults={
                  <div>
                    <p>Did you find what you were looking for?</p>
                    <p>No worries! Tell Expese.</p>
                    <Button>Product Request</Button>  {/* send request to the backend */}
                  </div>
                }
                style={{
  								width: "100%",
  								textAlign: "center", margin: "0 auto"
  							}}
  						/>
  					</div>

  			</ReactiveBase>


      );
    }
  }

  render(){
    const todoState = this.props.mappedTodoState;
    const productState = this.props.mappedProductState;
    const todos = todoState.todos;
    const editTodo = todoState.todoToEdit;
    const products = productState.products;

    var w = window.innerWidth + 'px';console.log(w)

    return(
      <div className="container productmain">

      { productState.isFetching
        ? this.renderLander()
        : this.renderProducts(products) }
      </div>
    );
  }
}
