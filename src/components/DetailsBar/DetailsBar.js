import React, { Component } from 'react'
import { Input, Menu, Segment } from 'semantic-ui-react'


export default class DetailsBar extends Component {
  state = { activeItem: 'home' }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    const { activeItem } = this.state

    return (
      <div>
        <Menu pointing>
          <Menu.Item name='Expeser Review' active={activeItem === 'Expeser Review'} onClick={this.handleItemClick} />
          <Menu.Item name='Whats in the Box' active={activeItem === 'Whats in the Box'} onClick={this.handleItemClick} />
          <Menu.Item name='Rating' active={activeItem === 'Rating'} onClick={this.handleItemClick} />
          <Menu.Item name='Manual' active={activeItem === 'Manual'} onClick={this.handleItemClick} />
        </Menu>

        <Segment>
          <img src='/assets/images/wireframe/paragraph.png' />
        </Segment>
        
      </div>
    )
  }
}
