// ./react-redux-client/src/reducers/todoReducer.js
const INITIAL_STATE = {
  products:[],
  product:null,
  isFetching: false,
  error: null,
  successMsg:null,
  showDeleteModal: false
}

export  const productReducer = (currentState = INITIAL_STATE, action) => {console.log(currentState)
console.log( ' action is : '); console.log(action);
  switch (action.type) {
    case 'FETCH_PRODUCTS_REQUEST':
          return {
            ...currentState,
            products:[],
            product:null,
            isFetching: true,
            error: null,
            successMsg:null
          }

    case 'FETCH_PRODUCTS_SUCCESS':
          return {
            ...currentState,
            products:action.products,
            product:null,
            isFetching: false,
            error: null,
            successMsg:action.message
          }

    case 'FETCH_PRODUCT_DETAIL_REQUEST':
          return {
            ...currentState,
            products:[],
            product:null,
            isFetching: true,
            error: null,
            successMsg:null
          }

    case 'FETCH_PRODUCTS_DETAIL_SUCCESS':
          return {
            ...currentState,
            products:currentState.products,
            product:action.product,
            isFetching: false,
            error: null,
            successMsg:action.message
          }

    case 'ADD_TO_QUEUE_REQUEST':
          return {
            ...currentState,
            products:[],
            product:null,
            isFetching: true,
            error: null,
            successMsg:null
          }

    case 'ADD_TO_QUEUE_SUCCESS':
          return {
            ...currentState,
            products: currentState.products,
            product: action.product,
            isFetching: false,
            error: null,
            successMsg: action.message
          }

    default:
       return currentState;

  }
}
