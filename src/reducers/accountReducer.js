const SET_LOGIN_PENDING = 'SET_LOGIN_PENDING';
const SET_LOGIN_SUCCESS = 'SET_LOGIN_SUCCESS';
const SET_LOGIN_ERROR = 'SET_LOGIN_ERROR';

const INITIAL_STATE = {
  isLoginSuccess: false,
  isLoginPending: true,
  loginError: null
}

export const accountReducer = (currentState = INITIAL_STATE, action) => {console.log(currentState)
console.log( ' actin is : '); console.log(action);
  console.log("state in reducer", currentState)
  switch (action.type) {
    case SET_LOGIN_PENDING:
      return Object.assign({}, currentState, {
        isLoginPending: action.isLoginPending
      });

    case SET_LOGIN_SUCCESS:
      return Object.assign({}, currentState, {
        isLoginSuccess: action.isLoginSuccess
      });

    case SET_LOGIN_ERROR:
      return Object.assign({}, currentState, {
        loginError: action.loginError
      });

    default:
      return currentState;
  }
}
