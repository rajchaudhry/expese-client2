// ./react-redux-client/src/reducers/appReducer.js
const INITIAL_STATE = {
  showSignUpModal: false,
}

const appReducer = (currentState = INITIAL_STATE, action) => {console.log(currentState)
  switch (action.type) {
    case 'SHOW_SIGNUP':
      return {
        ...currentState,showSignUpModal: !currentState.showSignUpModal,
      }
    default:
       return currentState;
  }
}

export default appReducer;
