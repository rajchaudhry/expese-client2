// ./react-redux-client/src/routes.js
import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './containers/App';
import Todos from './containers/Todos';
import Todo from './containers/Todo';
import Products from './containers/Products';
import ProductSearch from './containers/ProductSearch';
import ProductDetail from './containers/ProductDetail';
import Account from './containers/Account';
import CreditCard from './containers/CreditCards';
import Queue from './containers/Queue';
import { BrowserRouter as Router, Match} from 'react-router-dom'

//import About from './containers/About';
export default (
	<Router>
	  <Route path="/" component={App}>
		  <IndexRoute component={Todos} />
	    <Route path="/Todos" component={Todos} />
		<Route path="/queue" component={Queue} />
	    <Route path="/products" component={Products} />
	    <Route path="/productsearch" component={ProductSearch} />
	    <Route path="/product/:id" component={ProductDetail} />
	    <Route path="/account" component={Account} />
	    <Route path="/:id" component={Todo} />
	  </Route>
	</Router>
)
