// ./react-redux-client/src/actions/todoActions.js

// const apiUrl = "/api/";
const apiUrl = "https://0q0iv7efvh.execute-api.us-east-1.amazonaws.com/dev/";

export const toggleAddBook = () => {
  return {
    type: 'TOGGLE_ADD_TODO'
  }
}

//Async action fetchProductDetail
export const fetchProducts = () => {
  // Returns a dispatcher function
  // that dispatches an action at later time
  return (dispatch) => {

    dispatch(fetchProductsRequest());console.log("api ? ");console.log(apiUrl);
    // Returns a promise
    return fetch(apiUrl + 'products')
                .then(response => {console.log(response);
                  if(response.ok){
                    response.json().then(data => {console.log(data)
                      dispatch(fetchProductsSuccess(data.rows,data.message));
                    })
                  }
                  else{
                    response.json().then(error => {
                      dispatch(fetchProductsFailed(error));
                    })
                  }
                })


  }
}

export const fetchProductsRequest = () => {
  return {
    type:'FETCH_PRODUCTS_REQUEST'
  }
}

//Sync action
export const fetchProductsSuccess = (products,message) => {
  return {
    type: 'FETCH_PRODUCTS_SUCCESS',
    products: products,
    message: message,
    receivedAt: Date.now
  }
}

export const fetchProductsFailed = (error) => {
  return {
    type:'FETCH_PRODUCTS_FAILED',
    error
  }
}

//Async action fetchProductDetail
export const fetchProductDetail = (id) => {
  // Returns a dispatcher function
  // that dispatches an action at later time
  return (dispatch) => {

    dispatch(fetchProductDetailRequest());console.log("api ? " + id);console.log(apiUrl);
    // Returns a promise
    return fetch(apiUrl + 'product/'+id)
                .then(response => {console.log(response);
                  if(response.ok){
                    response.json().then(data => {console.log(data)
                      dispatch(fetchProductDetailSuccess(data.rows,data.message));
                    })
                  }
                  else{
                    response.json().then(error => {
                      dispatch(fetchProductDetailFailed(error));
                    })
                  }
                })


  }
}

export const fetchProductDetailRequest = () => {
  return {
    type:'FETCH_PRODUCTS_DETAIL_REQUESTT'
  }
}

//Sync action
export const fetchProductDetailSuccess = (product, message) => {
  return {
    type: 'FETCH_PRODUCTS_DETAIL_SUCCESS',
    product: product,
    message: message,
    receivedAt: Date.now
  }
}

export const fetchProductDetailFailed = (error) => {
  return {
    type:'FETCH_PRODUCT_DETAIL_FAILED',
    error
  }
}

export const showDeleteModal = (todoToDelete) => {
  return {
    type:'SHOW_DELETE_MODAL',
    todo:todoToDelete
  }
}

export const hideDeleteModal = () => {
  return {
    type:'HIDE_DELETE_MODAL'
  }
}

//Async action addToQueue
export const addToQueue = () => {
  // Returns a dispatcher function
  // that dispatches an action at later time
  return (dispatch) => {

    dispatch(addToQueueRequest());console.log("api ? ");console.log(apiUrl);
    // Returns a promise
    return fetch(apiUrl + 'queue/',
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          userId: 'user_id',
          productId: 'product_id',
        })
      },
    ).then(response => {console.log(response);
      if (response.ok) {
        response.json().then(data => {console.log(data)
          dispatch(addToQueueSuccess(data.rows, data.message));
        })
      }
      else {
        response.json().then(error => {
          dispatch(addToQueueFailed(error));
        })
      }
    })
  }
}

export const addToQueueRequest = () => {
  return {
    type:'ADD_TO_QUEUE_REQUEST'
  }
}

//Sync action
export const addToQueueSuccess = (product, message) => {
  return {
    type: 'ADD_TO_QUEUE_SUCCESS',
    products: product,
    message: message,
    receivedAt: Date.now
  }
}

export const addToQueueFailed = (error) => {
  return {
    type:'ADD_TO_QUEUE_FAILED',
    error
  }
}