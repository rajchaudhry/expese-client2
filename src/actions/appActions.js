// ./react-redux-client/src/actions/appActions.js

const apiUrl = 'https://randomuser.me/api';
export const toggleAddTodo = () => {
  return {
    type: 'TOGGLE_ADD_TODO'
  }
}

export const showSignUp = () => {
  return {
    type: 'SHOW_SIGNUP'
  }
}

export const toggleNewsletter = (name) => {
	return (dispatch) => {
		dispatch(addNewNameRequest(name));
   		return fetch(apiUrl, {
      		method:'post',
      		body: name,
    }).then(response => {
      if(response.ok){
        response.json().then(data => {console.log(data.name);
          dispatch(addNewNameRequestSuccess(data.name))
        })
      }
      else{
        response.json().then(error => {
          dispatch(addNewNameRequestFailed(error))
        })
      }
    })
  }
}

export const addNewNameRequest = (name) => {
  return {
    type: 'ADD_NEW_NAME_REQUEST',
    name
  }
}

export const addNewNameRequestSuccess = (name) => {
	return {
		type: 'ADD_NEW_NAME_REQUEST_SUCCESS',
		name
	}
}

export const addNewNameRequestFailed = (error) => {
	return {
		type: 'ADD_NEW_NAME_REQUEST_FAILED',
		error
	}
}