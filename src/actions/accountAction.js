const apiUrl = "https://0q0iv7efvh.execute-api.us-east-1.amazonaws.com/dev/";
const SET_LOGIN_PENDING = 'SET_LOGIN_PENDING';
const SET_LOGIN_SUCCESS = 'SET_LOGIN_SUCCESS';
const SET_LOGIN_ERROR = 'SET_LOGIN_ERROR';

export const toggleAddBook = () => {
  return {
    type: 'TOGGLE_ADD_TODO'
  }
}

/*
// Signup action 
export const postSignup = () => {
  // Returns a dispatcher function
  // that dispatches an action at later time
  return (dispatch) => {

    dispatch(fetchProductsRequest());console.log("api ? ");console.log(apiUrl);
    // Returns a promise
    return fetch(apiUrl + 'products')
                .then(response => {console.log(response);
                  if(response.ok){
                    response.json().then(data => {console.log(data)
                      dispatch(fetchProductsSuccess(data.rows,data.message));
                    })
                  }
                  else{
                    response.json().then(error => {
                      dispatch(fetchProductsFailed(error));
                    })
                  }
                })
  }
}
*/

/* example get method
function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
 
    return fetch('/users/' + _id, requestOptions).then(handleResponse);
}
*/

//Login action
export const Login = (username, password) => {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };

    // Returns a dispatcher function
    // that dispatches an action at later time
    return (dispatch) => {

    dispatch(setLoginPending(true));
    dispatch(setLoginSuccess(false));
    dispatch(setLoginError(null));

    dispatch(fetchProductsRequest());
    console.log("api ? ");
    console.log(apiUrl);
    // Returns a promise
 
    return fetch(apiUrl, requestOptions)
        .then(response => {console.log(response);
          //const result = response.json();
            if (!response.ok) {
              // just to test the successful login
              dispatch(setLoginSuccess(true));
              // uncomment below when api is ready
              /*
               result.then(error => {
               dispatch(fetchUserFailed(error));
               dispatch(setLoginError(error));
              })*/
            }
            // just for testing error message because api currently return html
            return null;
            //return response.json();
        })
        .then(user => {
            // login successful if there's a jwt token in the response
            if (user && user.token) {
                // store use details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('user', JSON.stringify(user));
                dispatch(setLoginSuccess(true));
            }
 
            return user;
        });
    }
}

//Signup action
export const Signup = (username, password) => {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };

    // Returns a dispatcher function
    // that dispatches an action at later time
    return (dispatch) => {

    dispatch(setLoginPending(true));
    dispatch(setLoginSuccess(false));
    dispatch(setLoginError(null));

    dispatch(fetchProductsRequest());console.log("api ? ");console.log(apiUrl);
    // Returns a promise
 
    return fetch(apiUrl, requestOptions)
        .then(response => {console.log(response);
          //const result = response.json();
            if (!response.ok) {
              // just to test the successful login
              dispatch(setLoginSuccess(true));
              // uncomment below when api is ready
              /*
               result.then(error => {
               dispatch(fetchUserFailed(error));
               dispatch(setLoginError(error));
              })*/
            }
            // just for testing error message because api currently return html
            return null;
            //return response.json();
        })
        .then(user => {
            // login successful if there's a jwt token in the response
            if (user && user.token) {
                // store use details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('user', JSON.stringify(user));
                dispatch(setLoginSuccess(true));
            }
 
            return user;
        });
    }
}

function setLoginPending(isLoginPending) {
  return {
    type: SET_LOGIN_PENDING,
    isLoginPending
  };
}

function setLoginSuccess(isLoginSuccess) {
  return {
    type: SET_LOGIN_SUCCESS,
    isLoginSuccess
  };
}

function setLoginError(loginError) {
  return {
    type: SET_LOGIN_ERROR,
    loginError
  }
}

export const fetchProductsRequest = () => {
  return {
    type:'FETCH_PRODUCTS_REQUEST'
  }
}

//Sync action
export const fetchProductsSuccess = (products,message) => {
  return {
    type: 'FETCH_PRODUCTS_SUCCESS',
    products: products,
    message: message,
    receivedAt: Date.now
  }
}

export const fetchUserFailed = (error) => {
  return {
    type:'FETCH_USER_FAILED',
    error
  }
}

//Async action fetchProductDetail
export const fetchProductDetail = (id) => {
  // Returns a dispatcher function
  // that dispatches an action at later time
  return (dispatch) => {

    dispatch(fetchProductDetailRequest());console.log("api ? " + id);console.log(apiUrl);
    // Returns a promise
    return fetch(apiUrl + 'product/'+id)
                .then(response => {console.log(response);
                  if(response.ok){
                    response.json().then(data => {console.log(data)
                      dispatch(fetchProductDetailSuccess(data.rows,data.message));
                    })
                  }
                  else{
                    response.json().then(error => {
                      dispatch(fetchProductDetailFailed(error));
                    })
                  }
                })


  }
}

export const fetchProductDetailRequest = () => {
  return {
    type:'FETCH_PRODUCTS_DETAIL_REQUESTT'
  }
}

//Sync action
export const fetchProductDetailSuccess = (product, message) => {
  return {
    type: 'FETCH_PRODUCTS_DETAIL_SUCCESS',
    product: product,
    message: message,
    receivedAt: Date.now
  }
}

export const fetchProductDetailFailed = (error) => {
  return {
    type:'FETCH_PRODUCT_DETAIL_FAILED',
    error
  }
}

export const showDeleteModal = (todoToDelete) => {
  return {
    type:'SHOW_DELETE_MODAL',
    todo:todoToDelete
  }
}

export const hideDeleteModal = () => {
  return {
    type:'HIDE_DELETE_MODAL'
  }
}