// ./react-redux-client/src/actions/queueActions.js
const apiUrl = "https://0q0iv7efvh.execute-api.us-east-1.amazonaws.com/dev/";

//Async action fetchQueue
export const fetchQueue = (userId) => {
  // Returns a dispatcher function
  // that dispatches an action at later time
  return (dispatch) => {
    dispatch(fetchQueueRequest());console.log("api ? ");console.log(apiUrl);
    // Returns a promise
    return fetch(apiUrl + 'queue/' + userId)
      .then(response => {console.log(response);
      if(response.ok){
        response.json().then(data => {console.log(data)
          dispatch(fetchQueueSuccess(data.rows,data.message));
        })
      }
      else{
        response.json().then(error => {
          dispatch(fetchQueueFailed(error));
        })
      }
    })
  }
}

export const fetchQueueRequest = () => {
  return {
    type:'FETCH_QUEUE_REQUEST'
  }
}

//Sync action
export const fetchQueueSuccess = (products,message) => {
  return {
    type: 'FETCH_QUEUE_SUCCESS',
    products: products,
    message: message,
    receivedAt: Date.now
  }
}

export const fetchQueueFailed = (error) => {
  return {
    type:'FETCH_QUEUE_FAILED',
    error
  }
}

//Async action postQueue
export const postQueue = (userId, products) => {
  // Returns a dispatcher function
  // that dispatches an action at later time
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      userId: userId,
      products: products
    })
  };

  return (dispatch) => {
    dispatch(postQueueRequest());console.log("api ? " + userId);console.log(apiUrl);
    // Returns a promise
    return fetch(apiUrl + 'queue/', requestOptions)
      .then(response => {console.log(response);
      if(response.ok){
        response.json().then(data => {console.log(data)
          dispatch(postQueueSuccess(data.message));
        })
      }
      else{
        response.json().then(error => {
          dispatch(postQueueFailed(error));
        })
      }
    })
  }
}

export const postQueueRequest = () => {
  return {
    type:'POST_QUEUE_REQUEST'
  }
}

//Sync action
export const postQueueSuccess = (message) => {
  return {
    type: 'POST_QUEUE_SUCCESS',
    message: message,
    receivedAt: Date.now
  }
}

export const postQueueFailed = (error) => {
  return {
    type:'POST_QUEUE_FAILED',
    error
  }
}