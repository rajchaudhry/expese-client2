// ./react-redux-client/src/containers/Todos.js
import { connect } from 'react-redux';
// import * as todoActions from '../actions/todoActions';
import * as productActions from '../actions/productActions';
import Products from '../components/Products/Products';

// map state from store to props
const mapStateToProps = (state,ownProps) => {
  return {
    //you can now say this.props.mappedAppSate
    mappedTodoState: state.todoState,
    mappedProductState: state.productState
  }
}

// map actions to props
const mapDispatchToProps = (dispatch) => {
  return {
    //you can now say this.props.mappedAppActions
    fetchProducts: () => dispatch(productActions.fetchProducts())
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Products);
