import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
//import * as accountAction from '../actions/accountAction';
import {Signup} from '../actions/accountAction';
import SignupModal from '../components/Account/SignupModal';

// map state from store to props
const mapStateToProps = (state,ownProps) => {
  return {
    //you can now say this.props.mappedAppSate
    mappedSignupState: state.signupState
  }
}

// map actions to props
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({Signup},dispatch)

}
    //you can now say this.props.mappedAppActions
    //login: data => dispatch(accountAction.login(data))

export default connect(mapStateToProps,mapDispatchToProps)(SignupModal);
