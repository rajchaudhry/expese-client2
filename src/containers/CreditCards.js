import { connect } from 'react-redux';
import CreditCard from '../components/CreditCard/index.js';

// map state from store to props
const mapStateToProps = (state,ownProps) => {
  return {
    //you can now say this.props.mappedAppSate
    mappedProductState: state.productState
  }
}

// map actions to props
const mapDispatchToProps = (dispatch) => {
  return {
    //you can now say this.props.mappedAppActions
    //fetchProductDetail: productId => dispatch(productActions.fetchProductDetail(productId))
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(CreditCard);