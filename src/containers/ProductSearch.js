// ./react-redux-client/src/containers/ProductSearch.js
import { connect } from 'react-redux';
import * as productActions from '../actions/productActions';
import ProductSearch from '../components/ProductSearch/ProductSearch';

// map state from store to props
const mapStateToProps = (state,ownProps) => {
  return {
    //you can now say this.props.mappedAppSate
    mappedTodoState: state.todoState,
    mappedProductState: state.productState
  }
}

// map actions to props
const mapDispatchToProps = (dispatch) => {
  return {
    //you can now say this.props.mappedAppActions
    fetchProducts: () => dispatch(productActions.fetchProducts()),
    addToQueue: () => dispatch(productActions.addToQueue())
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(ProductSearch);
