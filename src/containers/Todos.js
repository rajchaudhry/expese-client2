// ./react-redux-client/src/containers/Todos.js
import { connect } from 'react-redux';
import Lander from '../components/Lander';

// map state from store to props
const mapStateToProps = (state,ownProps) => {
  return {
    //you can now say this.props.mappedAppSate
    mappedTodoState: state.todoState
  }
}

export default connect(mapStateToProps)(Lander);
