// ./react-redux-client/src/containers/Todos.js
import { connect } from 'react-redux';
// import * as todoActions from '../actions/todoActions';
// import * as productActions from '../actions/productActions';
// import ProductDetail from '../components/ProductDetail/ProductDetail';
import About from '../components/About/About';

// map state from store to props
const mapStateToProps = (state,ownProps) => {
  return {
    //you can now say this.props.mappedAppSate
    mappedProductState: state.productState
  }
}

// map actions to props
const mapDispatchToProps = (dispatch) => {
  return {
    //you can now say this.props.mappedAppActions
    // fetchProductDetail: productId => dispatch(productActions.fetchProductDetail(productId))
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(About);
