// ./react-redux-client/src/containers/Queue.js
import { connect } from 'react-redux';
import * as queueActions from '../actions/queueActions';
import Queue from '../components/Queue/Queue';

// map state from store to props
const mapStateToProps = (state,ownProps) => {
  return {
    //you can now say this.props.mappedAppSate
    mappedProductState: state.productState
  }
}

// map actions to props
const mapDispatchToProps = (dispatch) => {
  return {
    fetchQueue: () => dispatch(queueActions.fetchQueue()),
    postQueue: () => dispatch(queueActions.postQueue())
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Queue);